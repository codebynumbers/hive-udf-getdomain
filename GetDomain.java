import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;
//import java.security.*;

/**
 * Calculate md5 of the string
 */
public final class GetDomain extends UDF {

    private static final Pattern REGEX_WWW = Pattern.compile("(?i)^www\\.");
    private static final Pattern REGEX_INTERNATIONAL_DOMAIN = Pattern.compile("(?i)\\.[a-z]{2,3}\\.[a-z]{2}$");
    private static final Pattern REGEX_DOT = Pattern.compile("\\.");

    public Text evaluate(final Text s) {
        if (s == null) {
                return null;
        }
        return new Text(getRootDomain(s.toString()));
    }

    /**
     * The root domain name
     *
     * @param domain The domain we want to look up
     * @return string representing the root domain
     */
    public static String getRootDomain(String domain) {

            // first, remove www.
            domain = removeWWW(domain);

            final List<String> parts = Arrays.asList(REGEX_DOT.split(domain));

            // Find the number of '.' separated chunks we need to look at
            final int l = parts.size();
            if (isDomainInternational(domain)) {
                    return parts.get(l - 3) + '.' + parts.get(l - 2) + '.' + parts.get(l - 1);
            }

            if (l < 2) {
                    return domain; // domain like "localhost"
            }
            return parts.get(l - 2) + '.' + parts.get(l - 1);
    }

    /**
     * Check if the domain looks like an international domain (suffix contains 2 parts)
     *
     * @return true iff the domain looks like an international domain
     */
    public static boolean isDomainInternational(final CharSequence domain) {
            return REGEX_INTERNATIONAL_DOMAIN.matcher(domain).find();
    }

    /**
     * Remove www. from the domain and return the resulting String
     *
     * @param domain The domain we want to remove www. from
     * @return String representation of the domain without www.
     */
    public static String removeWWW(final CharSequence domain) {
            return REGEX_WWW.matcher(domain).replaceFirst("");
    }

    public static void main(String[] args) {
        System.out.println(getRootDomain(args[0]));
    }
}
